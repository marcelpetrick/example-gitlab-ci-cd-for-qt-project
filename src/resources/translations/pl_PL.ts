<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>SampleItem</name>
    <message>
        <source>SAMLPE_TEXT</source>
        <translation type="vanished">Przykładowy tekst</translation>
    </message>
</context>
<context>
    <name>TranslationManager</name>
    <message>
        <location filename="../../tools/TranslationManager.cpp" line="76"/>
        <source>TARGET_LANGUAGE</source>
        <translation>Polski</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../../qml/main.qml" line="9"/>
        <source>Scythe Studio QML Desktop App</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="30"/>
        <source>CHANGE_LANGUAGE</source>
        <translation>Zmień język</translation>
    </message>
</context>
</TS>
